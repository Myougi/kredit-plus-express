<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Input Order KPX</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1eea9a48-81b2-4d68-8c41-3143b32d3f20</testSuiteGuid>
   <testCaseLink>
      <guid>de95db58-7ac5-426c-8e3d-7bdae00eb4ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/KPX/Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>edef44ce-dc08-446a-8344-b8365da9dffc</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>2-2</value>
         </iterationEntity>
         <testDataId>Data Files/Login</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>edef44ce-dc08-446a-8344-b8365da9dffc</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>0c7ad529-b0ab-47d5-af14-a44c1bee3b71</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>edef44ce-dc08-446a-8344-b8365da9dffc</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>f844cc25-16f7-457f-b27f-73d8e96ebaa3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d5134662-eb71-4863-b02c-1c7f603428bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/KPX/Order</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Order</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>KTP NO</value>
         <variableId>5551acc6-8056-49d2-86e6-a997337ca6b2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>TGL LAHIR</value>
         <variableId>25da00cd-b669-4413-b820-a170d0fc7ac2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>NAMA CUSTOMER</value>
         <variableId>15f54b62-6741-41f7-8b1f-77a6e0ade12d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>NAMA IBU KANDUNG</value>
         <variableId>d3eb50bd-1c61-47dd-966a-2b80fa1ff0e2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>NO HP</value>
         <variableId>0cc87cfa-999d-407c-889f-77af991ef5d8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>TEMPAT LAHIR</value>
         <variableId>92d12dfe-f3fc-43fe-aaa9-b8250b17c3ec</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>JENIS KELAMIN</value>
         <variableId>f987432d-bbed-43e0-a6ad-f6ce9efbc23b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>EMAIL</value>
         <variableId>8d35a13d-5a64-4f21-b2ca-03cd7a4f531d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>PENDIDIKAN</value>
         <variableId>ca00f9d9-8249-4506-9472-e9c5cfef2377</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>STATUS NIKAH</value>
         <variableId>3451c11e-bfde-4b9e-8615-58a031fc6601</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>JML TANGGUNGAN</value>
         <variableId>ad0074ba-954a-4f9e-84e6-01ead70fd65b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>STATUS TEMPAT TINGGAL</value>
         <variableId>ac5d6d23-2dcc-420b-92fb-ba5798365388</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>TGL MULAI TINGGAL</value>
         <variableId>38b37b3e-b8c3-45fa-8609-ebc2c2c84ed4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>ALAMAT</value>
         <variableId>0c4f5010-93f0-4b84-90f8-08131b10a7de</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>KOTA</value>
         <variableId>6c3eda23-bc0b-4c2e-86c0-76486496f57e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>KECAMATAN</value>
         <variableId>cff9f7ee-c041-4bda-83b9-00fb3a9ea196</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>KELURAHAN</value>
         <variableId>5e708c83-e790-4ecb-9aee-4b97e5301198</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>KODE POS</value>
         <variableId>b7cf56eb-c5cb-4ab8-9ac6-f53001adc215</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>PROFESI</value>
         <variableId>486e06b4-cd87-42a4-b7c1-180162e28036</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>TIPE PEKERJAAN</value>
         <variableId>a1b6f69f-d19f-4d25-86ad-88ecc9caca91</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>TGL MULAI KERJA</value>
         <variableId>26922c40-b61d-44b8-ad73-1e078885babc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>PENGHASILAN</value>
         <variableId>4336cc81-c29f-4d0f-a8de-22eb973a8cdb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>PENGHASILAN LAIN</value>
         <variableId>172fdf11-0310-45e1-bd47-9b5adbfb52e2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>NAMA PERUSAHAAN</value>
         <variableId>746d30e4-41e0-49a1-9bf5-5f463bd0ef91</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>PHONE AREA KANTOR</value>
         <variableId>08a28f13-5fda-4476-b8ea-7ba2222cff3f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>PHONE NUMBER KANTOR</value>
         <variableId>b8434f8a-8319-4a41-b07d-d71f3afd37c7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>NAMA EMERGENCY KONTAK</value>
         <variableId>4f1b3ea6-2581-45fa-aa0c-ec55108e0206</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddf05ca6-bc77-4fa8-afbb-a2281c26b03d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>NO EMERGENCY KONTAK</value>
         <variableId>659a0f3c-ab31-420a-9469-2445e4c69883</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4fb798c1-a520-4678-ba2e-13019da92b9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/KPX/Logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
