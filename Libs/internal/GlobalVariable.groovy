package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object KPX_Multi
     
    /**
     * <p></p>
     */
    public static Object KPGateway
     
    /**
     * <p></p>
     */
    public static Object KPX_PRODUCTION
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            KPX_Multi = selectedVariables['KPX_Multi']
            KPGateway = selectedVariables['KPGateway']
            KPX_PRODUCTION = selectedVariables['KPX_PRODUCTION']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
