import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.setText(findTestObject('KPX/Order/txt_KTP'), no_ktp)

WebUI.setText(findTestObject('KPX/Order/txt_TgLahir'), tgl_lahir)

WebUI.sendKeys(findTestObject('KPX/Order/txt_TgLahir'), Keys.chord(Keys.TAB))

WebUI.setText(findTestObject('KPX/Order/txt_NamaPemohon'), nama_customer)

WebUI.setText(findTestObject('KPX/Order/txt_NamaIbuKandung'), nama_ibu)

WebUI.setText(findTestObject('KPX/Order/txt_Handphone'), no_hp)

WebUI.sendKeys(findTestObject('KPX/Order/txt_Handphone'), Keys.chord(Keys.TAB))

WebUI.waitForElementNotVisible(findTestObject('KPX/Order/loading_message'), 10)

WebUI.setText(findTestObject('KPX/Order/txt_TempatLahir'), tempat_lahir)

if (jenis_kelamin.equals('Laki-Laki')) {
    WebUI.check(findTestObject('KPX/Order/chk_JenisKelamin', [('no') : 0]))
} else {
    WebUI.check(findTestObject('KPX/Order/chk_JenisKelamin', [('no') : 1]))
}

WebUI.setText(findTestObject('Object Repository/KPX/Order/txt_Email'), email)

WebUI.click(findTestObject('KPX/Order/span_Pendidikan'))

WebUI.setText(findTestObject('KPX/Order/txt_SearchDataDropdown'), pendidikan)

WebUI.sendKeys(findTestObject('KPX/Order/txt_SearchDataDropdown'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('KPX/Order/span_StatusNikah'))

WebUI.setText(findTestObject('KPX/Order/txt_SearchDataDropdown'), status_nikah)

WebUI.sendKeys(findTestObject('KPX/Order/txt_SearchDataDropdown'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('KPX/Order/txt_Tanggungan'), jml_tanggungan)

WebUI.click(findTestObject('KPX/Order/span_KepemilikanRumah'))

WebUI.setText(findTestObject('KPX/Order/txt_SearchDataDropdown'), kepemilikan_rumah)

WebUI.sendKeys(findTestObject('KPX/Order/txt_SearchDataDropdown'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('KPX/Order/txt_MulaiTinggal'), tgl_tinggal)

WebUI.sendKeys(findTestObject('KPX/Order/txt_MulaiTinggal'), Keys.chord(Keys.TAB))

WebUI.setText(findTestObject('KPX/Order/txt_Alamat'), alamat)

WebUI.click(findTestObject('KPX/Order/span_Kota'))

WebUI.setText(findTestObject('KPX/Order/txt_SearchDataDropdown'), kota)

WebUI.sendKeys(findTestObject('KPX/Order/txt_SearchDataDropdown'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('KPX/Order/span_Kecamatan'))

WebUI.setText(findTestObject('KPX/Order/txt_SearchDataDropdown'), kecamatan)

WebUI.sendKeys(findTestObject('KPX/Order/txt_SearchDataDropdown'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('KPX/Order/span_Kelurahan'))

WebUI.setText(findTestObject('KPX/Order/txt_SearchDataDropdown'), kelurahan)

WebUI.sendKeys(findTestObject('KPX/Order/txt_SearchDataDropdown'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('KPX/Order/btn_Next'))

WebUI.click(findTestObject('KPX/Order/span_Profesi'))

WebUI.setText(findTestObject('KPX/Order/txt_SearchDataDropdown'), profesi)

WebUI.sendKeys(findTestObject('KPX/Order/txt_SearchDataDropdown'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('KPX/Order/span_TipePekerjaan'))

WebUI.setText(findTestObject('KPX/Order/txt_SearchDataDropdown'), tipe_pekerjaan)

WebUI.sendKeys(findTestObject('KPX/Order/txt_SearchDataDropdown'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('KPX/Order/txt_TglMulaiKerja'), tgl_mulai_kerja)

WebUI.sendKeys(findTestObject('KPX/Order/txt_TglMulaiKerja'), Keys.chord(Keys.TAB))

WebUI.setText(findTestObject('KPX/Order/txt_Penghasilan'), penghasilan)

WebUI.setText(findTestObject('KPX/Order/txt_PengasilanLain'), penghasilan_lain)

WebUI.setText(findTestObject('KPX/Order/txt_NamaKantor'), nama_kantor)

WebUI.setText(findTestObject('KPX/Order/txt_PhoneAreaKantor'), phone_area_kantor)

WebUI.setText(findTestObject('KPX/Order/txt_PhoneNoKantor'), phone_number_kantor)

WebUI.setText(findTestObject('KPX/Order/txt_NamaEmergencyKontak'), nama_emergency_kontak)

WebUI.setText(findTestObject('KPX/Order/txt_PhoneEmergencyKontak'), no_emergency_kontak)

WebUI.executeJavaScript('alert(\'Mohon Upload Foto by Manual, anda hanya diberikan 1 menit untuk melakukan proses tersebut.\')', 
    [])

WebUI.delay(2)

WebUI.acceptAlert()

WebUI.delay(60)

WebUI.click(findTestObject('KPX/Order/btn_Next'))

WebUI.executeJavaScript('alert(\'Mohon lakukan penginputan barang secara manual.\')', [])

WebUI.delay(2)

WebUI.acceptAlert()

WebUI.delay(120, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/KPX/Order/btn_Submit'))

WebUI.delay(20)

WebUI.click(findTestObject('KPX/Order/btn_OTP_OK'))

WebUI.click(findTestObject('KPX/Order/btn_OK'))

